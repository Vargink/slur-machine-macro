# Slur machaine automated macro #

Automatically change the super offensive words into the super encoded version of what you want to say.

### Features ###

* Make the words you say super encoded so that (((people))) have no idea what you're talking about. 
* Automatically changes them to the current days encoded version of the word useing [slurmachines](http://slurmachine.fun/) api.
* Add additional words to the .ini file provided for easy and simple additions.

### Getting Started ###

You can either compile this yourself using [AutoIT](https://www.autoitscript.com/site/) or you can download the pre compilied version in our [downloads](https://bitbucket.org/Vargink/slur-machine-macro/downloads/)
All you need to do after this is simply run the app, if you want to add new slurs, you just need to open the slurs.ini and add them in there.
The app runs in your system tray and appears white when it is updating the slurs and black any other time.

### Current Limitations ###

If there is a abreviated version of a word, the longer version will not be able to be used. For example, mat and mattres. Since the abreveated version contains some of the string.
If being compiled from source it can only be compiled as a 32 bit app, this is due to the [BinaryCall.au3](https://www.autoitscript.com/forum/topic/162366-binarycall-udf-write-subroutines-in-c-call-in-autoit/)

### Sources ###

[Curl.au3](https://www.autoitscript.com/forum/topic/173067-curl-udf-autoit-binary-code-version-of-libcurl-with-ssl-support/)
[Request.au3](https://www.autoitscript.com/forum/topic/173129-request-udf-the-simplest-way-to-make-http-request/)
[hotstrings.au3](https://www.autoitscript.com/forum/topic/68422-hotstrings-string-hotkeys/)
[BinaryCall.au3](https://www.autoitscript.com/forum/topic/162366-binarycall-udf-write-subroutines-in-c-call-in-autoit/)
[Json.au3](https://www.autoitscript.com/forum/topic/148114-a-non-strict-json-udf-jsmn/)

### Who do I talk to? ###

* [Vargink](https://keybase.io/theguywho)