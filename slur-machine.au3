; Slurmachine cheat sheet macro
; 
; Made By Vargink
; replace your slurs with the latest code from the slur macheine
;
#include-once
#include <Array.au3>
#include "lib/BinaryCall.au3"
#include "lib/hotstrings.au3"
#include <Date.au3>
#include "lib/Curl.au3"
#include "lib/Request.au3"

Global $Slurs[1] = [0];
Global $Coded[1] = [0];

Global $Day;

Func Init()
    ; Get slurs from ini, get their encoded version and setup hotstrings for them
    TraySetIcon(@ScriptDir & "/ico/init.ico", 0);
    $Day = @MDAY;
    Local $slurid = 1;
    Local $CurrentSlur = IniRead(@ScriptDir & "\slurs.ini", "slurs", $slurid, "-1");
    While $CurrentSlur <> "-1"
        if($CurrentSlur <> "-1") Then
            Local $oReceived = Request("http://slurmachine.fun/encode/" & $CurrentSlur & "?date="& _DateToMonth(@MON) & "+"+@MDAY & "+" & @YEAR);
            Local $oJson = Json_Decode($oReceived);
            Local $encodeSlur = Json_ObjGet($oJson, "word");
            if $encodeSlur <> "fail" Then
                Local $iSlursNewIndex = _ArrayAdd($Slurs, $CurrentSlur);
                Local $iCodedNewIndex = _ArrayAdd($Coded, $encodeSlur);
                If $iSlursNewIndex <> -1 And $iCodedNewIndex <> -1 Then
                    $Slurs[0] = $iSlursNewIndex;
                    $Coded[0] = $iCodedNewIndex;
                    HotStringSet($Slurs[$iSlursNewIndex], "ConvertSlur");
                EndIf
            EndIf
        EndIf
        $slurid = $slurid + 1;
        $CurrentSlur = IniRead("slurs.ini", "slurs", $slurid, "-1");
    WEnd
    TraySetIcon(@ScriptDir & "/ico/dickshow.ico", 0);
    ; everything is setup, start the slur updater to check for a day change every minute.
    AdlibRegister("Update_slurs", 60000);
EndFunc 

Func ConvertSlur($hotstring)
    local $arrayID = _ArraySearch($Slurs, $hotstring);
    For $i = 1 To StringLen($Slurs[$arrayID]) Step +1
       send("{BACKSPACE}");
    Next
    send($Coded[$arrayID]);
EndFunc

Func Update_slurs()
    If @MDAY <> $Day Then
        TraySetIcon(@ScriptDir & "/ico/init.ico", 0);
        $Day = @MDAY;
        ; days changed update the current shit.
        For $i = 0 To $Slurs[0] Step +1
            Local $oReceived = Request("http://slurmachine.fun/encode/" & $Slurs[$i] & "?date="& _DateToMonth(@MON) & "+"+@MDAY & "+" & @YEAR);
            Local $oJson = Json_Decode($oReceived);
            Local $encodeSlur = Json_ObjGet($oJson, "word");
            if $encodeSlur <> "fail" Then
                $Coded[$i] = $encodeSlur;
            EndIf
        Next
        TraySetIcon(@ScriptDir & "/ico/dickshow.ico", 0);
    EndIf
EndFunc

Init();

While 1
    Sleep(100); An idle loop.
WEnd